#ifndef _CRS_HPP_
#define _CRS_HPP_

#include <vector>
#include <assert.h>
#include <fstream>
#include <iostream>

using namespace std;

class CRS
{
protected:
	vector<double> coef2;
	vector<int> row_ptr, col_ind;
	int n;

public:
	CRS() {};

	CRS(const char* fname) {
		ifstream f(fname);
		double d;
		if (f.good()) {
			string header;
			f >> header;
			if (header == "%%MatrixMarket") {
				f >> header;
				f >> header;
				f >> header;
				f >> header; // symmetric or general
				if (header != "symmetric") {
					cout << "Matrix must be symmetric" << endl;
					return;
				}
			} else {
				cout << "Invalid Matrix Market File" << endl;
				return;
			}

			// skip through rest of header
			while (f.peek() == '%' || f.peek() == '\n') {
				f.ignore(1024,'\n');
			}

			// read n,m from meta data line
			int nnz;
			f >> n; f >> n; f >> nnz;   // read n and m
			// ignores 4th number in meta line
			if (f.peek() != '\n')
				f >> d;

			int count = 0;
			bool twoColFile = true;
			bool firstLine = true;
			int row, col;
			int oldRow = -1;

			// read lines
			for (int i = 0; i < nnz; i++) {
				f >> col; col--;
				f >> row; row--;
				// read first data line to determine if 2 or 3 columns
				if (firstLine){
					if (f.peek() != '\n'){
						twoColFile = false;
						//cout << "reading three col file " << endl;
					} else {
						twoColFile = true;
						//cout << "reading two col file "   << endl;
					}
					firstLine = false;
				}
				if (!twoColFile){
					f >> d;
				}
				col_ind.push_back(col);
				while (oldRow < row) {
					row_ptr.push_back(count);
					oldRow++;
				}
				oldRow = row;
				count++;
			}
			row_ptr.push_back(count);
			coef2.resize(count);
			f.close();
		} else {
			cout << "Error Opening File: " << fname << endl;
		}
	}

	void print_crs() {
		cout << "coef2 = ";
		for(int i = 0; i < coef2.size(); i++)
			cout << coef2[i] << " ";

		cout << endl << "row_ptr   = ";
		for(int i = 0; i < row_ptr.size(); i++)
			cout << row_ptr[i] << " ";

		cout << endl << "col_ind = ";
		for(int i = 0; i < col_ind.size(); i++)
			cout << col_ind[i] << " ";

		cout << endl;
	};

	void print_crs_coord() {
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				cout << "(" << i << "," << col_ind[r] << ")" << endl;
			}
		}
	};

	double get2(const int i1, const int i2) const {
		assert(i1 >= 0 && i2 >= 0 && i1 < n  && i2 < n);
		int i, j;
		if (i1 <= i2) {
			i = i1;
			j = i2;
		} else {
			i = i2;
			j = i1;
		}
		for(int k = row_ptr[i]; k < row_ptr[i+1]; k++) {
			if(col_ind[k] == j){
				return coef2[k];
			}
		}
		return 0;
	};
};

#endif
