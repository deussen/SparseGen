#ifndef _CCS_HPP_
#define _CCS_HPP_

#include <vector>
#include <assert.h>
#include <fstream>
#include <iostream>
#include "CTS.hpp"
using namespace std;

class CCS : public CTS
{
protected:
	vector<double> coef4;
	vector<int> cube_ptr, l_ind;

public:
	CCS() : CTS() {};

	CCS(const char* fname) : CTS(fname) {
		int cube_cnt = 0;
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				//int j = col_ind[r];
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					cube_ptr.push_back(cube_cnt);
					//int k = k_ind[t];
					int a_ptr = tube_ptr[r];
					int b_ptr = row_ptr[k_ind[t]];
					int a = k_ind[a_ptr];
					int b = col_ind[b_ptr];
					while (a_ptr < tube_ptr[r+1] && b_ptr < row_ptr[k_ind[t]+1] /* && a <= k && b <= k*/) {
						if (a == b) {
							l_ind.push_back(a);
							cube_cnt++;
						}
						if (a <= b) {
							a_ptr++;
							a = k_ind[a_ptr];
						} else {
							b_ptr++;
							b = col_ind[b_ptr];
						}
					}
				}
			}
		}
		cube_ptr.push_back(cube_cnt);
		coef4.resize(cube_cnt);
	};

	void print_ccs() {
		print_cts();
		cout << "coef4 = ";
		for(int i = 0; i < coef4.size(); i++)
			cout << coef4[i] << " ";

		cout << endl << "cube_ptr = ";
		for(int i = 0; i < cube_ptr.size(); i++)
			cout << cube_ptr[i] << " ";

		cout << endl << "l_ind   = ";
		for(int i = 0; i < l_ind.size(); i++)
			cout << l_ind[i] << " ";

		cout << endl;
	};

	void print_ccs_coord() {
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				int j = col_ind[r];
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					int k = k_ind[t];
					for (int c = cube_ptr[t]; c < cube_ptr[t+1]; c++) {
						int l = l_ind[c];
						cout << "(" << i << "," << j << "," << k << "," << l << ")" << endl;
					}
				}
			}
		}
	};

	double get4(const int i1, const int i2, const int i3, const int i4) const {
		assert(i1 >= 0 && i2 >= 0 && i3 >= 0 && i4 >= 0 && i1 < n  && i2 < n && i3 < n && i4 < n);
		int i, j, k, l;
		if (i1 <= i2 && i2 <= i3 && i3 <= i4) {
			i = i1;
			j = i2;
			k = i3;
			l = i4;
		} else {
			vector<int> index(4);
			index[0] = i1;
			index[1] = i2;
			index[2] = i3;
			index[3] = i4;
			sort(index.begin(),index.end());
			i = index[0];
			j = index[1];
			k = index[2];
			l = index[3];
		}

		for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
			if (col_ind[r] == j) {
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					if (k_ind[t] == k) {
						for (int c = cube_ptr[t]; c < cube_ptr[t+1]; c++) {
							if (l_ind[c] == l) {
								return coef4[c];
							} else if (l_ind[c] > l) {
								return 0;
							}
						}
					} else if (k_ind[t] > k) {
						return 0;
					}
				}
			} else if (col_ind[r] > j) {
				return 0;
			}
		}
		return 0;
	}
};

#endif
