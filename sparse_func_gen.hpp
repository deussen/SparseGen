#ifndef _SFG_HPP_
#define _SFG_HPP_

#include <utility>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <functional>
#include <random>
#include <chrono>

#include "CCS.hpp"
using namespace std;

class SFG : public CCS {
public:
	~SFG() {}

	SFG() : CCS() {
		read();
	}

	SFG(const char* fname, double rho3 = 1.0, double rho4 = 1.0) : CCS(fname) {
		init_coef(rho3,rho4);
	}

	template <typename Tx, typename Ty>
	void f(const Tx *x, Ty &y, int n_eval = 1) {
		y = 0;
		for (int i_eval = 0; i_eval < n_eval; i_eval++) {
			for (int i = 0; i < n; i++) {
				for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
					int j = col_ind[r];
					y += coef2[r]*x[i]*x[j];
					for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
						int k = k_ind[t];
						y += coef3[t]*x[i]*x[j]*x[k];
						for (int c = cube_ptr[t]; c < cube_ptr[t+1]; c++) {
							int l = l_ind[c];
							y += coef4[c]*x[i]*x[j]*x[k]*x[l];
						}
					}
				}
			}
		}
		y /= n_eval;
	}

	int get_n() {
		return n;
	}

	//**************** Generate Function ***************//
private:
	void init_coef(double rho3 = 1.0, double rho4 = 1.0) {
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::default_random_engine gen(seed);
		std::uniform_real_distribution<double> dist(0.0,1.0);
		std::uniform_real_distribution<double> distP(-1.0,1.0);
		double z;
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				z = distP(gen);
				coef2[r] = z;
			}
		}
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					z = abs(floor(rho3-dist(gen)+1))*distP(gen);
					coef3[t] = z;
				}
			}
		}
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				int j = col_ind[r];
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					int k = k_ind[t];
					for (int c = cube_ptr[t]; c < cube_ptr[t+1]; c++) {
						int l = l_ind[c];
						if (get3(i,j,k) && get3(i,j,l) && get3(i,k,l) && get3(j,k,l)) {
							z = abs(floor(rho4-dist(gen)+1))*distP(gen);
							coef4[c] = z;
						}
					}
				}
			}
		}
	}

public:
	//******************** Sparsity ********************//

	void get_sparsity(bool **pH, bool ***pTD, bool ****pFD) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				pH[i][j] = false;
				for (int k = 0; k < n; k++) {
					pTD[i][j][k] = false;
					for (int l = 0; l < n; l++) {
						pFD[i][j][k][l] = false;
					}
				}
			}
		}
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				int j = col_ind[r];
				pH[i][j] = coef2[r];
				pH[j][i] = coef2[r];
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					int k = k_ind[t];
					pTD[i][j][k] = coef3[t];
					pTD[i][k][j] = coef3[t];
					pTD[j][i][k] = coef3[t];
					pTD[j][k][i] = coef3[t];
					pTD[k][i][j] = coef3[t];
					pTD[k][j][i] = coef3[t];
					for (int c = cube_ptr[t]; c < cube_ptr[t+1]; c++) {
						int l = l_ind[c];
						pFD[i][j][k][l] = coef4[c];
						pFD[i][j][l][k] = coef4[c];
						pFD[i][k][j][l] = coef4[c];
						pFD[i][k][l][j] = coef4[c];
						pFD[i][l][j][k] = coef4[c];
						pFD[i][l][k][j] = coef4[c];
						pFD[j][i][k][l] = coef4[c];
						pFD[j][i][l][k] = coef4[c];
						pFD[j][k][i][l] = coef4[c];
						pFD[j][k][l][i] = coef4[c];
						pFD[j][l][i][k] = coef4[c];
						pFD[j][l][k][i] = coef4[c];
						pFD[k][j][i][l] = coef4[c];
						pFD[k][j][l][i] = coef4[c];
						pFD[k][i][j][l] = coef4[c];
						pFD[k][i][l][j] = coef4[c];
						pFD[k][l][j][i] = coef4[c];
						pFD[k][l][i][j] = coef4[c];
						pFD[l][j][k][i] = coef4[c];
						pFD[l][j][i][k] = coef4[c];
						pFD[l][k][j][i] = coef4[c];
						pFD[l][k][i][j] = coef4[c];
						pFD[l][i][j][k] = coef4[c];
						pFD[l][i][k][j] = coef4[c];
					}
				}
			}
		}
	}

	void get_sparsity(unsigned int **pH, unsigned int ***pTD, unsigned int ****pFD) {
		for (int i = 0; i < n; i++) {
			vector<int> tmp2;
			tmp2.reserve(n);
			int nnz2 = 0;
			for (int j = 0; j < n; j++) {
				if (get2(i,j)) {
					nnz2++;
					tmp2.push_back(j);

					vector<int> tmp3;
					tmp3.reserve(n);
					int nnz3 = 0;
					for (int k = 0; k < n; k++) {
						if (get3(i,j,k)) {
							nnz3++;
							tmp3.push_back(k);
							vector<int> tmp4;
							tmp4.reserve(n);
							int nnz4 = 0;
							for (int l = 0; l < n; l++) {
								if (get4(i,j,k,l)) {
									nnz4++;
									tmp4.push_back(l);
								}
							}
							pFD[i][j][k] = new unsigned int[nnz4+1]();
							pFD[i][j][k][0] = nnz4;
							for (int l = 0; l < nnz4; l++) {
								pFD[i][j][k][l+1] = tmp4[l];
							}
						} else {
							pFD[i][j][k] = new unsigned int[1]();
							pFD[i][j][k][0] = 0;
						}
					}
					pTD[i][j] = new unsigned int[nnz3+1]();
					pTD[i][j][0] = nnz3;
					for (int k = 0; k < nnz3; k++) {
						pTD[i][j][k+1] = tmp3[k];
					}
				} else {
					pTD[i][j] = new unsigned int[1]();
					pTD[i][j][0] = 0;
					for (int k = 0; k < n; k++) {
						pFD[i][j][k] = new unsigned int[1]();
						pFD[i][j][k][0] = 0;
					}
				}
			}
			pH[i] = new unsigned int[nnz2+1]();
			pH[i][0] = nnz2;
			for (int j = 0; j < nnz2; j++) {
				pH[i][j+1] = tmp2[j];
			}
		}
	}

	//******************** Print ********************//

	void print() {
		bool first = true;
		cout << "f(x) = ";
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				int j = col_ind[r];
				if (!first && coef2[r] > 0) {
					cout << " +";
				} else {
					cout << " ";
				}
				first = false;
				cout << coef2[r] << "*x["<<i<<"]*x["<<j<<"]" << endl;
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					int k = k_ind[t];
					if (coef3[t]) {
						if (coef3[t] > 0) {
							cout << " +";
						} else {
							cout << " ";
						}
						cout << coef3[t] << "*x["<<i<<"]*x["<<j<<"]*x["<<k<<"]" << endl;
					}
					for (int c = cube_ptr[t]; c < cube_ptr[t+1]; c++) {
						int l = l_ind[c];
						if (coef4[c]) {
							if (coef4[c] > 0) {
								cout << " +";
							} else {
								cout << " ";
							}
							cout << coef4[c] << "*x["<<i<<"]*x["<<j<<"]*x["<<k<<"]*x["<<l<<"]" << endl;
						}
					}
				}
			}
		}
		cout << endl;
	}

	//******************** Write and Read ********************//

	void write() {
		ofstream out;
		out.open("function.csv");
		out << n << endl;
		out << coef2.size() << " ";
		for (double var : coef2) out << var << " ";
		out << endl << row_ptr.size() << " ";
		for (int var : row_ptr) out << var << " ";
		out << endl << col_ind.size() << " ";
		for (int var : col_ind) out << var << " ";
		out << endl << coef3.size() << " ";
		for (double var : coef3) out << var << " ";
		out << endl << tube_ptr.size() << " ";
		for (int var : tube_ptr) out << var << " ";
		out << endl << k_ind.size() << " ";
		for (int var : k_ind) out << var << " ";
		out << endl << coef4.size() << " ";
		for (double var : coef4) out << var << " ";
		out << endl << cube_ptr.size() << " ";
		for (int var : cube_ptr) out << var << " ";
		out << endl << l_ind.size() << " ";
		for (int var : l_ind) out << var << " ";
		out << endl;
		out.close();
	}

	void read() {
		const char* fname = "function.csv";
		ifstream in(fname);
		if (in.good()) {
			int i;
			in >> i;
			this->n = i;
			in >> i;
			coef2.resize(i);
			for (double &var : coef2) in >> var;
			in >> i;
			row_ptr.resize(i);
			for (int &var : row_ptr) in >> var;
			in >> i;
			col_ind.resize(i);
			for (int &var : col_ind) in >> var;
			in >> i;
			coef3.resize(i);
			for (double &var : coef3) in >> var;
			in >> i;
			tube_ptr.resize(i);
			for (int &var : tube_ptr) in >> var;
			in >> i;
			k_ind.resize(i);
			for (int &var : k_ind) in >> var;
			in >> i;
			coef4.resize(i);
			for (double &var : coef4) in >> var;
			in >> i;
			cube_ptr.resize(i);
			for (int &var : cube_ptr) in >> var;
			in >> i;
			l_ind.resize(i);
			for (int &var : l_ind) in >> var;
		} else {
			cout << "Error Opening File: " << fname << endl;
		}
		in.close();
	}
};
#endif
