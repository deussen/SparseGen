#ifndef _CTS_HPP_
#define _CTS_HPP_

#include <vector>
#include <assert.h>
#include <fstream>
#include <iostream>
#include "CRS.hpp"
using namespace std;

class CTS : public CRS
{
protected:
	vector<double> coef3;
	vector<int> tube_ptr, k_ind;

public:
	CTS() : CRS() {};

	CTS(const char* fname) : CRS(fname) {
		int tube_cnt = 0;
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				tube_ptr.push_back(tube_cnt);
				int a_ptr = row_ptr[i];
				int b_ptr = row_ptr[col_ind[r]];
				int a = col_ind[a_ptr];
				int b = col_ind[b_ptr];
				while (a_ptr < row_ptr[i+1] && b_ptr < row_ptr[col_ind[r]+1]) {
					if (a == b) {
						k_ind.push_back(a);
						tube_cnt++;
					}
					if (a <= b) {
						a_ptr++;
						a = col_ind[a_ptr];
					} else {
						b_ptr++;
						b = col_ind[b_ptr];
					}
				}
			}
		}
		tube_ptr.push_back(tube_cnt);
		coef3.resize(tube_cnt);
	};

	void print_cts() {
		print_crs();
		cout << "coef3 = ";
		for (int i = 0; i < coef3.size(); i++)
			cout << coef3[i] << " ";

		cout << endl << "tube_ptr = ";
		for (int i = 0; i < tube_ptr.size(); i++)
			cout << tube_ptr[i] << " ";

		cout << endl << "k_ind   = ";
		for (int i = 0; i < k_ind.size(); i++)
			cout << k_ind[i] << " ";

		cout << endl;
	};

	void print_cts_coord() {
		for (int i = 0; i < n; i++) {
			for (int r = row_ptr[i]; r < row_ptr[i+1]; r++) {
				int j = col_ind[r];
				for (int t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					int k = k_ind[t];
					cout << "(" << i << "," << j << "," << k << ")" << endl;
				}
			}
		}
	};

	double get3(const int i1, const int i2, const int i3) const {
		// assume i <= j <= k
		// return value at coordinate (i,j,k)
		assert(i1 >= 0 && i2 >= 0 && i3 >= 0 && i1 < n  && i2 < n && i3 < n);
		int i, j, k;

		if (i1 <= i2 && i2 <= i3) {
			i = i1;
			j = i2;
			k = i3;
		} else {
			vector<int> index(3);
			index[0] = i1;
			index[1] = i2;
			index[2] = i3;
			sort(index.begin(),index.end());
			i = index[0];
			j = index[1];
			k = index[2];
		}
		int r = 0, t = 0;
		for (r = row_ptr[i]; r < row_ptr[i+1]; r++) {
			if (col_ind[r] == j) {
				for (t = tube_ptr[r]; t < tube_ptr[r+1]; t++) {
					if (k_ind[t] == k) {
						return coef3[t];
					} else if (k_ind[t] > k) {
						return 0;
					}
				}
			} else if (col_ind[r] > j) {
				return 0;
			}
		}
		return 0;
	};
};

#endif
