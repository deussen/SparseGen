#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
using namespace std;

#include "sparse_func_gen.hpp"

int main(int argc, char* argv[]) {
	double rho3, rho4;
	char* fname;
	if (argc == 4) {
		fname = argv[1];
		rho3 = atof(argv[2]);
		rho4 = atof(argv[3]);
	} else {
		cout << "./opt path_to_mat rho3 rho4\n";
		return 0;
	}

	SFG *func = new SFG(fname,rho3,rho4);
	func->print();
	func->print_ccs();

//	CCS *func = new CCS(fname);
//	func->print_crs_coord();
//	func->print_cts_coord();
//	func->print_ccs_coord();

	return 0;
}
